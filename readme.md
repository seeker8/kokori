![Kokori logo](images/kokori_logo.png)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

---

Kokori is a PHP-based flat-file CMS based on [Koseven](https://github.com/koseven/koseven), and released under BSD license.
It was built for [solarus-games.org](https://www.solarus-games.org/) and relies on [solarus-website-pages](https://gitlab.com/solarus-games/solarus-website-pages/) for content.

This repo also contains front-end code such as CSS and JavaScript.
In addition, it defines content types and views which are then utilized by the data repository.

To get started with Kokori, [see the docs.](https://gitlab.com/solarus-games/solarus-website-pages/blob/dev/guidelines/installation.md)
