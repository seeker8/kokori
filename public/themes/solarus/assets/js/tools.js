$(document).ready(function() {
    // Preview
    var previewNode = $('#tools-preview .preview');
    $('#tools-preview textarea').on('keyup', function() {
        var textareaNode = $(this);
        var content = textareaNode.val();
        $.ajax({
           url: '../ajax/tools_get_preview',
            method: 'POST',
            dataType: 'json',
            data: {
               content: content
           },
            success: function(data) {
               console.dir(data);
                previewNode.html(data['content']);
            }
        });
    });
});