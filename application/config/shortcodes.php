<?php
return array(
  'alert' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'dismiss' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'title' => array(
        'default_value' => false,
        'required' => false
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      )
    )
  ),
  'align' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => true
      )
    )
  ),
  'article-thumbnail' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation' => 'article',
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'article-metas' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation' => 'article',
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'article-footer' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation' => 'article',
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'article-listing' => array(
    'callback' => array(
      'type' => 'listing',
      'entity_type' => 'article'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'column-width' => array(
        'default_value' => 6,
        'required' => false
      ),
      'count' => array(
        'default_value' => -1,
        'required' => false
      ),
      'message-empty' => array(
        'default_value' => false,
        'required' => false
      ),
      'order' => array(
        'default_value' => "DESC",
        'required' => false
      ),
      'orderby' => array(
        'default_value' => "creation_date",
        'required' => false
      )
    )
  ),
  'article-recents' => array(
    'callback' => array(
      'type' => 'listing',
      'entity_type' => 'article'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'count' => array(
        'default_value' => -1,
        'required' => false
      ),
      'order' => array(
        'default_value' => "DESC",
        'required' => false
      ),
      'orderby' => array(
        'default_value' => "creation_date",
        'required' => false
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'badge' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      )
    )
  ),
  'bitcoin' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'link' => array(
        'default_value' => false,
        'required' => true
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      )
    )
  ),
  'book-chapter-listing' => array(
    'callback' => array(
      'type' => 'tree',
      'entity_type' => 'book-chapter'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      ),
      'count' => array(
        'default_value' => -1,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'message-empty' => array(
        'default_value' => false,
        'required' => false
      ),
      'order' => array(
        'default_value' => "ASC",
        'required' => false
      ),
      'orderby' => array(
        'default_value' => "name",
        'required' => false
      )
    )
  ),
  'book-chapter-navigation' => array(
    'callback' => array(
      'type' => 'tree_pagination',
      'entity_type' => 'book-chapter'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      ),
      'text-previous' => array(
        'default_value' => false,
        'required' => true
      ),
      'text-next' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'book-listing' => array(
    'callback' => array(
      'type' => 'listing',
      'entity_type' => 'book'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'count' => array(
        'default_value' => -1,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'message-empty' => array(
        'default_value' => false,
        'required' => false
      ),
      'order' => array(
        'default_value' => "ASC",
        'required' => false
      ),
      'orderby' => array(
        'default_value' => "name",
        'required' => false
      ),
      'section' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'book-side-tree' => array(
    'callback' => array(
      'type' => 'tree',
      'entity_type' => 'book-chapter'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'box' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'dismiss' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'box-highlight' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'dismiss' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'button' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'outline' => array(
        'default_value' => false,
        'required' => false
      ),
      'size' => array(
        'default_value' => 'md',
        'required' => false
      ),
      'target' => array(
        'default_value' => '_parent',
        'required' => false
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      ),
      'url' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'button-download' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'category' => array(
        'default_value' => false,
        'required' => true
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'outline' => array(
        'default_value' => false,
        'required' => false
      ),
      'modal-title' => array(
        'default_value' => false,
        'required' => true
      ),
      'modal-button-label' => array(
        'default_value' => false,
        'required' => true
      ),
      'size' => array(
        'default_value' => 'md',
        'required' => false
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      )
    )
  ),
  'button-highlight' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'subtitle' => array(
        'default_value' => false,
        'required' => false
      ),
      'target' => array(
        'default_value' => '_parent',
        'required' => false
      ),
      'url' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'column' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'width' => array(
        'default_value' => 'auto',
        'required' => false
      )
    )
  ),
  'container' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'layout' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'cover' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'is-parallax' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'donation' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'link' => array(
        'default_value' => false,
        'required' => true
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      )
    )
  ),
  'entity-button-download' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'label-disabled' => array(
        'default_value' => false,
        'required' => true
      ),
      'modal-title' => array(
        'default_value' => false,
        'required' => true
      ),
      'modal-button-label' => array(
        'default_value' => false,
        'required' => true
      ),
      'outline' => array(
        'default_value' => false,
        'required' => false
      ),
      'size' => array(
        'default_value' => 'md',
        'required' => false
      ),
      'target' => array(
        'default_value' => '_parent',
        'required' => false
      ),
      'title' => array(
        'default_value' => false,
        'required' => false
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      ),
      'url' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'entity-filter' => array(
    'callback' => array(
      'type' => 'filter'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'entity-type' => array(
        'default_value' => false,
        'required' => true
      ),
      'filter' => array(
        'default_value' => false,
        'required' => true
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'entity-search' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'text-placeholder' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'entity-sorting' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'label-sorting-alpha' => array(
        'default_value' => false,
        'required' => true
      ),
      'label-sorting-date' => array(
        'default_value' => false,
        'required' => true
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      ),
    )
  ),
  'form-contact' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'button-label' => array(
        'default_value' => false,
        'required' => true
      ),
      'captcha-sitekey' => array(
        'default_value' => false,
        'required' => true
      ),
      'email-label' => array(
        'default_value' => false,
        'required' => true
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'message-error' => array(
        'default_value' => false,
        'required' => true
      ),
      'message-label' => array(
        'default_value' => false,
        'required' => true
      ),
      'message-success' => array(
        'default_value' => false,
        'required' => true
      ),
      'name-label' => array(
        'default_value' => false,
        'required' => true
      ),
      'subject-label' => array(
        'default_value' => false,
        'required' => true
      ),
      'to-email' => array(
        'default_value' => false,
        'required' => true
      ),
      'to-name' => array(
        'default_value' => false,
        'required' => true
      ),
    )
  ),
  'game-cover' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'game-extras' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'game-featured' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      ),
      'title' => array(
        'default_value' => false,
        'required' => false
      ),
      'button-label' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'game-icon' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation' => 'game',
        'relation_type' => 'entity',
        'required' => true
      ),
      'size' => array(
        'default_value' => '16x16',
        'required' => true
      )
    )
  ),
  'game-listing' => array(
    'callback' => array(
      'type' => 'listing',
      'entity_type' => 'game'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'count' => array(
        'default_value' => -1,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'message-empty' => array(
        'default_value' => false,
        'required' => false
      ),
      'order' => array(
        'default_value' => "ASC",
        'required' => false
      ),
      'orderby' => array(
        'default_value' => "name",
        'required' => false
      )
    )
  ),
  'game-logo' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation' => 'game',
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'game-logo-author' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation' => 'game',
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'icon-solarus' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'required' => true
      ),
      'size' => array(
        'default_value' => 64,
        'required' => false
      )
    )
  ),
  'image' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'url' => array(
        'default_value' => '',
        'required' => true
      ),
      'alt' => array(
        'default_value' => '',
        'required' => false
      ),
      'modal' => array(
        'default_value' => true,
        'required' => false
      )
    )
  ),
  'medias-gallery' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation' => 'all',
        'relation_type' => 'entity',
        'required' => true
      ),
      'nb-items-visible' => array(
        'default_value' => 4,
        'required' => false
      ),
      'title' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'highlight' => array(
    'content' => array(
      'default_value' => true,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'hr' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'width' => array(
        'default_value' => 100,
        'required' => false
      )
    )
  ),
  'icon' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'animated' => array(
        'default_value' => false,
        'required' => false
      ),
      'bordered' => array(
        'default_value' => false,
        'required' => false
      ),
      'category' => array(
        'default_value' => 'fas',
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'required' => true
      ),
      'inversed' => array(
        'default_value' => false,
        'required' => false
      ),
      'mask' => array(
        'default_value' => false,
        'required' => false
      ),
      'size' => array(
        'default_value' => "lg",
        'required' => false
      ),
      'transform' => array(
        'default_value' => false,
        'required' => false
      ),
      'type' => array(
        'default_value' => "primary",
        'required' => false
      )
    )
  ),
  'jumbotron' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'lead' => array(
        'default_value' => false,
        'required' => false
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'link' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => false
      ),
      'target' => array(
        'default_value' => '_parent',
        'required' => false
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      ),
      'url' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'model' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'context-id' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'paypal' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'currency' => array(
        'default_value' => "EUR",
        'required' => false
      ),
      'email' => array(
        'default_value' => false,
        'required' => true
      ),
      'icon' => array(
        'default_value' => false,
        'relation' => 'icon',
        'relation_type' => 'shortcode',
        'required' => false
      ),
      'label' => array(
        'default_value' => false,
        'required' => true
      ),
      'name' => array(
        'default_value' => false,
        'required' => true
      ),
      'subject' => array(
        'default_value' => "",
        'required' => false
      ),
      'title' => array(
        'default_value' => false,
        'required' => true
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      )
    )
  ),
  'progressbar' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'animated' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'height' => array(
        'default_value' => 20,
        'required' => false
      ),
      'striped' => array(
        'default_value' => false,
        'required' => false
      ),
      'type' => array(
        'default_value' => 'primary',
        'required' => false
      ),
      'width' => array(
        'default_value' => 100,
        'required' => false
      )
    )
  ),
  'resource-pack-listing' => array(
    'callback' => array(
      'type' => 'listing',
      'entity_type' => 'resource-pack'
    ),
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'count' => array(
        'default_value' => -1,
        'required' => false
      ),
      'message-empty' => array(
        'default_value' => false,
        'required' => false
      ),
      'order' => array(
        'default_value' => "ASC",
        'required' => false
      ),
      'orderby' => array(
        'default_value' => "name",
        'required' => false
      )
    )
  ),
  'row' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'search' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'count' => array(
        'default_value' => -1,
        'required' => true
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'empty-text' => array(
        'default_value' => false,
        'required' => true
      ),
      'link-text' => array(
        'default_value' => false,
        'required' => true
      ),
      'search' => array(
        'default_value' => false,
        'required' => false
      ),
      'scope' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'small' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'space' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'thickness' => array(
        'default_value' => 30,
        'required' => false
      ),
      'orientation' => array(
        'default_value' => 'horizontal',
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'summary' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'level-min' => array(
        'default_value' => false,
        'required' => false
      ),
      'level-max' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'table' => array(
    'content' => array(
      'default_value' => true,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'table-row' => array(
    'content' => array(
      'default_value' => true,
      'is_active' => true,
      'required' => true
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'table-column' => array(
    'content' => array(
      'default_value' => true,
      'is_active' => true,
      'required' => false
    ),
    'atts' => array(
      'colspan' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'is-header' => array(
        'default_value' => false,
        'required' => false
      ),
      'rowspan' => array(
        'default_value' => false,
        'required' => false
      )
    )
  ),
  'thumbnail' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'relation_type' => 'entity',
        'required' => true
      )
    )
  ),
  'twitch' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'allowfullscreen' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'user-presentation' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'description' => array(
        'default_value' => false,
        'required' => true
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'image' => array(
        'default_value' => false,
        'required' => true
      ),
      'link-label' => array(
        'default_value' => false,
        'required' => false
      ),
      'link-target' => array(
        'default_value' => false,
        'required' => false
      ),
      'link-title' => array(
        'default_value' => false,
        'required' => false
      ),
      'link-url' => array(
        'default_value' => false,
        'required' => false
      ),
      'name' => array(
        'default_value' => false,
        'required' => true
      )
    )
  ),
  'youtube' => array(
    'content' => array(
      'default_value' => false,
      'is_active' => false,
      'required' => false
    ),
    'atts' => array(
      'allowfullscreen' => array(
        'default_value' => false,
        'required' => false
      ),
      'effect-in' => array(
        'default_value' => false,
        'required' => false
      ),
      'id' => array(
        'default_value' => false,
        'required' => true
      )
    )
  )
);