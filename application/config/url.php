<?php

return array(

	'trusted_hosts' => array(
		'solarus.benjamin',
		'localhost',
		'localhost:8000',
		'localhost:8080',
		'localhost:8888',
	),

);
