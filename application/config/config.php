<?php
return array(
  'cache' => false,
  'captcha' => array(
    'secret_key' => false
  ),
  'default_language' => 'en',
  'dirs' => array(
    'data' => 'data',
    'writable' => APPPATH .'writable'
  ),
  'languages' => array(
    'en' => array(
      'name' => 'English'
    ),
    'fr' => array(
      'name' => 'Français'
    ),
  )
);