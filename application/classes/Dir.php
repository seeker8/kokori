<?php

class Dir
{

  private $config;
  private $path;

  public function __construct($path = false)
  {

    $this->config = Kohana::$config->load('config');
    $this->path = $this->set_path($path);

  } //EOCo

  public static function factory($path = false)
  {
    return new Dir($path);

  } //EOM

  public function set_path($path = false)
  {
    if ($path == false) {
      return false;
    }
    $this->path = $path;

    return $this->path;

  } //EOM

  public function get_path()
  {

    return $this->path;

  } //EOM

  public function load($ext = array('json'))
  {
    if ($this->path == false) {
      return false;
    }
    $path = $this->path;
    if (is_dir($path) == false) {
      return false;
    }
    $files = Dir::browse_dir($path, $ext);

    return $files;

  } //EOM

  public static function browse_dir($folder, $ext = array('json'), $recursive = true)
  {
    $files = array();
    $dir = opendir($folder);
    while ($file = readdir($dir)) {
      if ($file == '.' || $file == '..') continue;
      if (is_dir($folder . '/' . $file)) {
        if ($recursive == true)
          $files = array_merge($files, Dir::browse_dir($folder . '/' . $file, $ext));
      } else {
        foreach ($ext as $v) {
          if (strtolower($v) == strtolower(substr($file, -strlen($v)))) {
            $files[] = $folder . '/' . $file;
            break;
          }
        }
      }
    }
    closedir($dir);

    return $files;
  }

} //EOC
