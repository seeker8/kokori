<?php
require_once("Libraries/parsedown/Parsedown.php");

class Utils
{

  public static function factory()
  {
    return new Utils();

  } //EOM

  public static function apply_format_fields_boolean($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }

    $value = (bool)$field['value'];

    return $value;

  } //EOM

  public static function apply_format_fields_category($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (is_array($field['value']) == false) {
      $field['value'] = array($field['value']);
    }

    return $field['value'];

  } //EOM

  public static function apply_format_fields_category_render($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (is_array($field['value']) == false) {
      $field['value'] = array($field['value']);
    }
    $value = '<span class="field-category">';
    foreach ($field['value'] as $current_value) {
      $value .= '<span>';
      $value .= '<a href="#">';
      $value .= $current_value;
      $value .= '</a>';
      $value .= '</span>';
    }
    $value .= '</span>';

    return $value;

  } //EOM

  public static function apply_format_fields_category_icon($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (is_array($field['value']) == false) {
      $field['value'] = array($field['value']);
    }

    return $field['value'];

  } //EOM

  public static function apply_format_fields_category_icon_render($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (is_array($field['value']) == false) {
      $field['value'] = array($field['value']);
    }
    $value = '<span class="field-category-icon">';
    foreach ($field['value'] as $current_value) {
      $value .= '<span>';
      $value .= '<a href="#">';
      $value .= '<i class="fa fa-' . $current_value . '"></i>';
      $value .= '</a>';
      $value .= '</span>';
    }
    $value .= '</span>';

    return $value;

  } //EOM

  public static function apply_format_fields_content($field = false, $entity_path = false, $conversion = true, $entity_type = 'entity')
  {

    if ($entity_path == false) {
      return false;
    }
    if ($field == false) {
      return false;
    }
    $value = $field['value'];
    $value = Utils::convert_content($value, $entity_path, $conversion, $entity_type);

    return $value;

  } //EOM

  public static function apply_format_fields_content_render($field = false, $entity_path = false, $conversion = true, $entity_type = 'entity')
  {

    return Utils::apply_format_fields_string($field, $entity_path, $conversion, $entity_type);

  } //EOM

  public static function apply_format_fields_date($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (empty($field['value'])) {
      $field['value'] = date('Y-m-d');
    }

    return $field['value'];

  } //EOM

  public static function apply_format_fields_date_render($field = false, $entity_path = false)
  {


    if ($field == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    // Load Main config page
    $path = $config['dirs']['data'] . '/config.json';
    $config_page = File::factory($path)->load();
    $language = $config_page['languages'][I18n::lang()];
    if (empty($field['value'])) {
      $value = date($language['format_date']);
    } else {
      $time = strtotime($field['value']);
      $value = date($language['format_date'], $time);
    }

    return $value;

  } //EOM

  public static function apply_format_fields_downloads($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (empty($field['value'])) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = '/' . $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $entity_path;
    foreach ($field['value'] as $key => $value) {
      if (Utils::is_absolute_url($value['file']) == false && !empty($value['file'])) {
        $field['value'][$key]['file'] = dirname($path) . '/' . $value['file'];
      }
    }
    // Search default download
    $value = $field['value'];
    $default = false;
    foreach ($field['value'] as $k => $v) {
      if ($v["platform"] == 'any') {
        $default = $v;
        //unset($value[$k]);
      }
    }

    if ($default == false) {
      $os = Utils::getOSType();
      foreach ($field['value'] as $k => $v) {
        if ($v["platform"] == $os) {
          $default = $v;
          break;
          // unset($value[$k]);
        }
      }
    }

    if ($default == false) {
      $default = $field['value'][0];
      //unset($value[0]);
    }

    $filePathItems = explode('/', $entity_path);
    unset($filePathItems[count($filePathItems) - 1]);
    $file_path = implode('/', $filePathItems);
    if ($default) {
      if (empty($file_path)) {
        $file_path_md = $config['dirs']['data'] . '/' . I18n::lang() . '/downloads/' . $default['content'];;
      } else {
        $file_path_md = $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $file_path . '/' . $default['content'];
      }
      $content = File::factory($file_path_md)->load();
      $default['content'] = Utils::convert_content($content);
    }
    $others = array();
    foreach ($value as $k => $v) {
      if (empty($file_path)) {
        $file_path_md = $config['dirs']['data'] . '/' . I18n::lang() . '/downloads/' . $v['content'];
      } else {
        $file_path_md = $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $file_path . '/' . $v['content'];
      }
      $content = File::factory($file_path_md)->load();
      $v['content'] = Utils::convert_content($content);
      $others[] = $v;
    }
    $value = array(
      'default' => $default,
      'others' => $others
    );

    return $value;

  } //EOM

  public static function apply_format_fields_image($field = false, $entity_path = false, $entity_delegate_path = false)
  {
    if ($field == false) {
      return false;
    }
    if (empty($field['value'])) {
      return false;
    }
    $config = Kohana::$config->load('config');
    if ($entity_delegate_path) {
      $entity_path = $entity_delegate_path;
    }
    $path = '/' . $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $entity_path;
    $value = dirname($path) . '/' . $field['value'];

    return $value;

  } //EOM

  public static function apply_format_fields_integer($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (empty($field['value'])) {
      return false;
    }
    $value = (int)$field['value'];

    return $value;

  } //EOM

  public static function apply_format_fields_images($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (empty($field['value'])) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = '/' . $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $entity_path;
    foreach ($field['value'] as $key => $value) {
      $field['value'][$key] = dirname($path) . '/' . $value;
    }

    return $field['value'];

  } //EOM

  public static function apply_format_fields_object($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (is_array($field['value']) == false) {
      $field['value'] = array($field['value']);
    }

    return $field['value'];

  } //EOM


  public static function apply_format_fields_object_render($field = false, $entity_path = false)
  {

    return false;

  } //EOM

  public static function apply_format_fields_media($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = '/' . $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $entity_path;
    foreach ($field['value'] as $k => $v) {
      if (isset($v["thumbnail"])) {
        $field['value'][$k]['thumbnail'] = dirname($path) . '/' . $field['value'][$k]['thumbnail'];
      }
      if ($v["type"] == "image" && isset($v["image"])) {
        $field['value'][$k]['url'] = dirname($path) . '/' . $field['value'][$k]['image'];
      }
      if ($v["type"] == "youtube" && isset($v["id"])) {
        $field['value'][$k]['url'] = 'https://www.youtube.com/watch?v=' . $v['id'];
      }
    }

    return $field['value'];

  } //EOM

  public static function apply_format_fields_string($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    $value = false;
    if (empty($field['value']) == false) {
      $value = (string)$field['value'];
    }

    return $value;

  } //EOM

  public static function apply_format_fields_string_render($field = false, $entity_path = false)
  {

    return Utils::apply_format_fields_string($field, $entity_path);

  } //EOM

  public static function apply_format_fields_url($field = false, $entity_path = false)
  {

    if ($field == false) {
      return false;
    }
    if (empty($field['value'])) {
      return false;
    }
    if (Utils::is_absolute_url($field['value'])) {
      return $field['value'];
    }
    $value = '/' . I18n::lang() . '/' . $field['value'];

    return $value;

  } //EOM

  public static function apply_format_fields_url_render($field = false, $entity_path = false)
  {

    return Utils::apply_format_fields_url($field, $entity_path);

  } //EOM

  public static function convert_content($content = false, $entity_path = false, $conversion = true, $entity_type = "model")
  {

    if ($content == false) {
      return false;
    }
    if (is_array($content)) {
      return false;
    }
    if ($conversion == false) {
      return $content;
    }
    // Titles level 1 conversion
    preg_match_all('/.*\n=+/m', $content, $matches);
    foreach ($matches[0] as $k => $v) {
      $line = str_replace('=', '', $v);
      $line = str_replace("\n", '', $line);
      $content = str_replace($v, '#' . $line, $content);
    }
    // Titles level 2 conversion
    /*
    preg_match_all('/.*\n-+/m', $content, $matches);
    foreach ($matches[0] as $k => $v) {
      $line = str_replace('-', '', $v);
      $line = str_replace("\n", '', $line);
      $content = str_replace($v, '##' . $line, $content);
    }
    */
    $config = Kohana::$config->load('config');
    // Replace variables
    $entity = Entity::factory($entity_path)->get_entity(false);
    if ($entity) {
      foreach ($entity as $k => $v) {
        $types = array("GET", "POST", "");
        foreach ($types as $type) {
          switch ($type) {
            case 'GET':
              $variable = '{GET:' . $k . '}';
              $variableValue = Arr::get($_GET, $k);
              break;
            case 'POST':
              $variable = '{POST:' . $k . '}';
              $variableValue = Arr::get($_POST, $k);
              break;
            default:
              $variable = '{' . $k . '}';
              if (!in_array($v["structure"]["type"], array("media", "downloads"))) {
                if (isset($v['render'])) {
                  $variableValue = $v["render"];
                  if (is_array($variableValue)) {
                    $variableValue = implode(' - ', $variableValue);
                  }
                } elseif (isset($v['value']) && !is_array($v['value'])) {
                  $variableValue = $v["value"];
                }
              } else {
                $variableValue = false;
              }
          }
          if ($variableValue) {
            $content = str_replace($variable, $variableValue, $content);
          }
        }
      }
    }
    $context = Session::instance()->get('context');
    $parsedown = new Parsedown();
    $parsedown->setUrlsLinked(false);
    $parsedown->setStrictMode(true);
    $content = $parsedown->parse($content);
    $content = str_replace('<p>[', '[', $content);
    $content = str_replace(']</p>', ']', $content);
    //Todo transform urls
    //Links
    /*
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
    if (false && preg_match_all("/$regexp/siU", $content, $matches)) {
      foreach ($matches[2] as $match) {
        $character = substr($match, 0, 1);
        if ($character != '#' && Utils::is_absolute_url($match) == false) {
          $base_path = $config['dirs']['data'] . '/entities/' . $context['language'];
          $base_path = $base_path . '/' . $context['entity_type'] . '/';
          $path = realpath($base_path . $match);
          $dirname = dirname($path);
          $filename = basename($path, '.md');
          $filenameItems = explode('.', $filename);
          if (count($filenameItems) == 2) {
            $file = File::factory($dirname . '/' . $filenameItems[0] . '.json')->load();
            if ($file == false) {
              continue;
            }
            $slug = $context['language'] . '/' . $file['slug'];
            $file = File::factory($dirname . '/' . $filename . '.json')->load();
            if ($file == false) {
              continue;
            }
            $slug = $slug . '/' . $file['slug'];
          } else {
            $file = File::factory($dirname . '/' . $filename . '.json')->load();
            if ($file == false) {
              continue;
            }
            $slug = $context['language'] . '/' . $file['slug'];
          }
          $content = str_replace($match, $slug, $content);
        }
      }
    }
    */
    // Images
    $regexp = "<img\s[^>]*src=(\"??)([^\" >]*?)\\1[^>]*>";
    if (preg_match_all("/$regexp/siU", $content, $matches)) {
      foreach ($matches[2] as $match) {
        if (Utils::is_absolute_url($match) == false) {
          $path = '/' . $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $entity_path;
          $url = dirname($path) . '/' . $match;
          $content = str_replace($match, $url, $content);
        }
      }
    }
    $content = Shortcode::factory()->parse_content($content, $entity_path, $entity_type);
    $pattern = "/<p[^>]*><\\/p[^>]*>/";
    //$pattern = "/<[^\/>]*>([\s]?)*<\/[^>]*>/";  use this pattern to remove any empty tag
    $content = preg_replace($pattern, '', $content);

    return $content;

  } //EOM

  public static function check_filter_url($type = false, $value = false)
  {

    if ($type == false) {
      return false;
    }
    if ($value == false) {
      return false;
    }
    $filter_url = Arr::get($_GET, $type, false);
    if ($filter_url == false) {
      return true;
    }
    $items = explode(',', $filter_url);
    if (in_array($value, $items)) {
      return true;
    }

  } //EOM

  public static function get_class_filters($entity = false)
  {

    if ($entity == false) {
      return false;
    }
    $classes = '';
    foreach ($entity as $k => $v) {
      if (isset($v['structure'])) {
        if ($v['structure']['type'] == 'category' && is_array($v['value'])) {
          foreach ($v['value'] as $ks => $vs) {
            if (empty($classes) == false) {
              $classes .= ' ';
            }
            $classes .= $k . '-' . Utils::slugify($vs);
          }
        }
      }
    }

    return $classes;

  } //EOM

  public static function get_file_extension($filename = false)
  {

    if ($filename == false) {
      return false;
    }
    return substr(strrchr($filename, '.'), 1);

  } //EOM

  public static function get_shortcode_atts($content)
  {

    preg_match_all('/([^ ]*)=(\'([^\']*)\'|\"([^\"]*)\"|([^ ]*))/', trim($content), $c);
    list($dummy, $keys, $values) = array_values($c);
    $c = array();
    foreach ($keys as $key => $value) {
      $value = trim($values[$key], "\"'");
      $type = is_numeric($value) ? 'int' : 'string';
      $type = in_array(strtolower($value), array('true', 'false')) ? 'bool' : $type;
      switch ($type) {
        case 'int':
          $value = (int)$value;
          break;
        case 'bool':
          $value = strtolower($value) == 'true';
        default:
          $value = (string)$value;
          break;
      }
      $c[$keys[$key]] = $value;
    }
    return $c;
  }

  public static function get_shortcode_alias_regex($shortcodes = null)
  {
    $tagregexp = join('|', array_map('preg_quote', $shortcodes));

    // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag()
    // Also, see shortcode_unautop() and shortcode.js.
    return
      '\\['                              // Opening bracket
      . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
      . "($tagregexp)_[0-9]+"                     // 2: Shortcode name
      . '(?![\\w-])';                       // Not followed by word character or hyphen;                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
  }

  public static function get_shortcode_regex($shortcodes = null)
  {
    $tagregexp = join('|', array_map('preg_quote', $shortcodes));

    // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag()
    // Also, see shortcode_unautop() and shortcode.js.
    return
      '\\['                              // Opening bracket
      . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
      . "($tagregexp)"                     // 2: Shortcode name
      . '(?![\\w-])'                       // Not followed by word character or hyphen
      . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
      . '[^\\]\\/]*'                   // Not a closing bracket or forward slash
      . '(?:'
      . '\\/(?!\\])'               // A forward slash not followed by a closing bracket
      . '[^\\]\\/]*'               // Not a closing bracket or forward slash
      . ')*?'
      . ')'
      . '(?:'
      . '(\\/)'                        // 4: Self closing tag ...
      . '\\]'                          // ... and closing bracket
      . '|'
      . '\\]'                          // Closing bracket
      . '(?:'
      . '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
      . '[^\\[]*+'             // Not an opening bracket
      . '(?:'
      . '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
      . '[^\\[]*+'         // Not an opening bracket
      . ')*+'
      . ')'
      . '\\[\\/\\2\\]'             // Closing shortcode tag
      . ')?'
      . ')'
      . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
  }

  public static function getOS()
  {

    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $os_platform = "Unknown OS Platform";

    $os_array = array(
      '/windows nt 10/i' => 'Windows 10',
      '/windows nt 6.3/i' => 'Windows 8.1',
      '/windows nt 6.2/i' => 'Windows 8',
      '/windows nt 6.1/i' => 'Windows 7',
      '/windows nt 6.0/i' => 'Windows Vista',
      '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
      '/windows nt 5.1/i' => 'Windows XP',
      '/windows xp/i' => 'Windows XP',
      '/windows nt 5.0/i' => 'Windows 2000',
      '/windows me/i' => 'Windows ME',
      '/win98/i' => 'Windows 98',
      '/win95/i' => 'Windows 95',
      '/win16/i' => 'Windows 3.11',
      '/macintosh|mac os x/i' => 'Mac OS X',
      '/mac_powerpc/i' => 'Mac OS 9',
      '/linux/i' => 'Linux',
      '/x11/i' => 'Linux',
      '/ubuntu/i' => 'Ubuntu',
      '/iphone/i' => 'iPhone',
      '/ipod/i' => 'iPod',
      '/ipad/i' => 'iPad',
      '/android/i' => 'Android',
      '/blackberry/i' => 'BlackBerry',
      '/webos/i' => 'Mobile',
      '/openbsd/i' => 'OpenBSD',
      '/freebsd/i' => 'FreeBSD'
    );

    foreach ($os_array as $regex => $value)
      if (preg_match($regex, $user_agent))
        $os_platform = $value;

    return $os_platform;

  }

  public static function startsWith($string, $substring)
  {
    return (substr($string, 0, strlen($substring)) === $substring);
  }

  public static function endsWith($string, $substring)
  {
    return (substr($string, strlen($string) - strlen($substring), strlen($string)) === $substring);
  }

  public static function getOSType()
  {

    $actual_os = Utils::getOS();
    $os_type = $actual_os;

    if (Utils::startsWith($actual_os, 'Windows')) {
      $os_type = 'Windows';
    } elseif (Utils::startsWith($actual_os, 'Mac OS')) {
      $os_type = 'macOS';
    } elseif (Utils::endsWith($actual_os, 'BSD')) {
      $os_type = 'BSD';
    }

    return $os_type;

  }

  public static function get_browser()
  {

    global $user_agent;

    $browser = "Unknown Browser";

    $browser_array = array(
      '/msie/i' => 'Internet Explorer',
      '/firefox/i' => 'Firefox',
      '/safari/i' => 'Safari',
      '/chrome/i' => 'Chrome',
      '/edge/i' => 'Edge',
      '/opera/i' => 'Opera',
      '/netscape/i' => 'Netscape',
      '/maxthon/i' => 'Maxthon',
      '/konqueror/i' => 'Konqueror',
      '/mobile/i' => 'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value)
      if (preg_match($regex, $user_agent))
        $browser = $value;

    return $browser;

  } //EOM

  public static function get_platform_icon($platform_id)
  {
    $platform_id = strtolower(trim($platform_id));
    switch ($platform_id) {
      case 'win':
      case 'windows':
        return 'windows';
      case 'mac':
      case 'macos':
      case 'os x':
      case 'osx':
      case 'ios':
        return 'apple';
      case 'ubuntu':
        return 'ubuntu';
      case 'android':
        return 'android';
      case 'raspberry-pi':
      case 'raspberry pi':
      case 'raspberrypi':
      case 'raspberry':
      case 'raspbian':
        return 'raspberry-pi';
      case 'opensuse':
      case 'suse':
        return 'suse';
      case 'fedora':
        return 'fedora';
      case 'linux':
      case 'debian':
        return 'linux';
      default:
        return '';
    }
  } //EOM

  public static function print_platform_icon_html($platform_id)
  {
    $icon = Utils::get_platform_icon($platform_id);
    if ($icon != '') {
      echo '<i class="fab fa-' . $icon . '"></i>';
    }
  }

  public static function str_replace_first($needle, $replacement, $haystack)
  {
    $needle_start = strpos($haystack, $needle);
    $needle_end = $needle_start + strlen($needle);
    if ($needle_start !== false) {
      $to_replace = substr($haystack, 0, $needle_end);
      return str_replace($needle, $replacement, $to_replace) . substr($haystack, $needle_end);
    } else
      return $haystack;

  } //EOM

  public static function str_replace_last($search, $replace, $str)
  {
    if (($pos = strrpos($str, $search)) !== false) {
      $search_length = strlen($search);
      $str = substr_replace($str, $replace, $pos, $search_length);
    }
    return $str;
  }

  public static function is_home()
  {
    // Issue with pages like /"aaa": should not return true
    if (Session::instance()->get('slug') == '/') {
      return true;
    }

    return false;
  } //EOM

  public static function is_absolute_url($url)
  {
    if (substr($url, 0, 7) == 'http://' || substr($url, 0, 8) == 'https://') {
      return true;
    }
    return false;
  }

  static public function slugify($text = false)
  {

    if ($text == false) {
      return false;
    }
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    // trim
    $text = trim($text, '-');
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    // lowercase
    $text = strtolower($text);
    if (empty($text)) {
      return 'n-a';
    }

    return $text;
  }

  function get_remote_file_size($url, $formatSize = true, $useHead = true)
  {
    $ch = curl_init($url);
    curl_setopt_array($ch, array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_FOLLOWLOCATION => 1,
      CURLOPT_SSL_VERIFYPEER => 0,
      CURLOPT_NOBODY => 1,
    ));
    if (false !== $useHead) {
      curl_setopt($ch, CURLOPT_NOBODY, 1);
    }
    curl_exec($ch);
    // content-length of download (in bytes), read from Content-Length: field
    $clen = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
    curl_close($ch);
    // cannot retrieve file size, return "-1"
    if (!$clen) {
      return -1;
    }
    if (!$formatSize) {
      return $clen; // return size in bytes
    }
    $size = $clen;
    switch ($clen) {
      case $clen < 1024:
        $size = $clen . ' B';
        break;
      case $clen < 1048576:
        $size = round($clen / 1024, 2) . ' KiB';
        break;
      case $clen < 1073741824:
        $size = round($clen / 1048576, 2) . ' MiB';
        break;
      case $clen < 1099511627776:
        $size = round($clen / 1073741824, 2) . ' GiB';
        break;
    }
    return $size; // return formatted size
  } // EOM
} //EOC
