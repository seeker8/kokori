<?php

class Shortcode
{

  private $config;
  private $shortcode;
  private $aliases;

  public function __construct()
  {

    $this->config = Kohana::$config->load('config');
    $this->shortcodes = Kohana::$config->load('shortcodes');
    $this->aliases = array();

  } //EOCo

  public static function factory()
  {
    return new Shortcode();

  } //EOM

  public function parse_content($content = false, $entity_path = false, $entity_type = "entity")
  {
    $shortcodes = array();
    foreach ($this->shortcodes as $k => $v) {
      $shortcodes[] = $k;
    }
    $regex = Utils::get_shortcode_alias_regex($shortcodes);
    if (preg_match_all('/' . $regex . '/s', $content, $matches)) {
      foreach ($matches[0] as $alias) {
        $alias = str_replace("[", "", $alias);
        $alias_items = explode('_', $alias);
        unset($alias_items[count($alias_items) - 1]);
        $shortcode = implode('_', $alias_items);
        $this->aliases[$alias] = $shortcode;
        $this->shortcodes[$alias] = $this->shortcodes[$shortcode];
      }
    }
    $regex = Utils::get_shortcode_regex($shortcodes);
    if (preg_match_all('/' . $regex . '/s', $content, $matches)) {
      $shortcodes = array();
      foreach ($matches[0] as $key => $value) {
        $hasClosedTag = false;
        if (strpos($matches[0][$key], '][/' . $matches[2][$key] . ']')) {
          $hasClosedTag = true;
        }
        $shortcodes[$value] = array(
          'name' => $matches[2][$key],
          'content' => Shortcode::factory()->parse_content($matches[5][$key], $entity_path),
          'atts' => Utils::get_shortcode_atts($matches[3][$key]),
          'has_closed_tag' => $hasClosedTag
        );
      }
      if (count($shortcodes) > 0) {
        foreach ($shortcodes as $key => $value) {
          $errors = $this->validate_shortcode($value, $entity_path);
          if (count($errors) == 0) {
            $shortcode_view = $this->get_shortcode($value, $entity_path, $entity_type);
            $content = str_replace($key, $shortcode_view, $content);
          } else {
            $shortcode_view = View::factory('front/shortcode_errors', array('data' => $value, 'errors' => $errors));
            $content = str_replace($key, $shortcode_view, $content);
          }
        }
      }
    }
    return $content;

  } //EOM

  public function get_shortcode($data, $entity_path, $entity_type)
  {

    $shortcode = $this->shortcodes[$data['name']];
    if (isset($data['atts']['icon'])) {
      $shortcodeIcon = $this->shortcodes['icon'];
      $attsIcon = array();
      foreach ($shortcodeIcon['atts'] as $k => $v) {
        if ($k == 'icon') {
          continue;
        }
        $attsIcon['icon-' . $k] = $v;
      }
      $shortcode['atts'] = array_merge($shortcode['atts'], $attsIcon);
    }
    foreach ($data['atts'] as $key => $value) {
      if (isset($shortcode['atts'][$key]) == false) {
        unset($data['atts'][$key]);
      }
    }
    if (isset($shortcode['atts'])) {
      foreach ($shortcode['atts'] as $key => $value) {
        if (isset($data['atts'][$key]) == false) {
          $data['atts'][$key] = $value['default_value'];
        } else {
          if (isset($value['relation_type'])) {
            switch ($value['relation_type']) {
              case 'entity':
                $entity_path = Entity::search_entity_path_from_id($data['atts'][$key]);
                if ($entity_path == false) {
                  return false;
                }
                $entity = Entity::factory($entity_path)->get_entity();
                $data['entity'] = $entity;
                break;
              case 'shortcode':
                $dataRelation = array(
                  'name' => $value['relation'],
                  'atts' => array(
                    $value['relation'] => $data['atts'][$value['relation']]
                  )
                );
                foreach ($data['atts'] as $k => $v) {
                  if (strpos($k, $value['relation'] . '-') === 0) {
                    $key_attribute = str_replace($value['relation'] . "-", "", $k);
                    $dataRelation['atts'][$key_attribute] = $v;
                  }
                }
                $data['atts'][$key] = $this->get_shortcode($dataRelation, $entity_path, $entity_type);
                break;
            }
          }
        }
      }
    }
    if (isset($shortcode['callback'])) {
      switch ($shortcode['callback']['type']) {
        case 'listing':
          if (!isset($data['atts']['section'])) {
            $data['atts']['section'] = false;
          }
          $data['listing'] = Entity::get_listing($shortcode['callback']['entity_type'], $data['atts']['count'], $data['atts']['orderby'], $data['atts']['order'], $data['atts']['section']);
          break;
        case 'tree':
          if (!isset($data['atts']['type'])) {
            $data['atts']['type'] = false;
          }
          $data['tree'] = Entity::get_tree($shortcode['callback']['entity_type'], $data['atts']['id']);
          break;
        case 'tree_pagination':
          if (!isset($data['atts']['type'])) {
            $data['atts']['type'] = false;
          }
          $data['pagination'] = Entity::get_tree_pagination($shortcode['callback']['entity_type'], $data['atts']['id']);
          break;
        case 'filter':
          $data['filters'] = Entity::get_filters($data['atts']['entity-type'], $data['atts']['filter']);
          break;
      }
    }
    $name = $data['name'];
    if (isset($this->aliases[$data['name']])) {
      $name = $this->aliases[$data['name']];
    }
    $name = str_replace("-", "_", $name);
    $method = 'get_shortcode_' . $name;
    if (method_exists($this, $method)) {
      $data = $this->{$method}($data, $entity_path, $entity_type);
    }
    $shortcode_view = View::factory('front/shortcodes/' . $name, array('data' => $data));

    return $shortcode_view;

  } //EOM

  public function validate_shortcode($data = array(), $entity_path = false)
  {

    $errors = array();
    if (isset($this->shortcodes[$data['name']]) == false) {
      $errors[] = array(
        'message' => "The shortcode " . $data['name'] . " does not exist.",
        'file' => $entity_path
      );
    }
    if (isset($this->shortcodes[$data['name']])) {
      if ($this->shortcodes[$data['name']]["content"]["is_active"] == false && $this->shortcodes[$data['name']]["content"]["required"] == false && $data['has_closed_tag']) {
        $errors[] = array(
          'message' => "This shortcode should not have a closing tag.",
          'file' => $entity_path
        );
      }
      if ($this->shortcodes[$data['name']]["content"]["required"] == true && empty($data["content"])) {
        $errors[] = array(
          'message' => "The content of this shortcode is empty.",
          'file' => $entity_path
        );
      }
      if (isset($this->shortcodes[$data['name']]["atts"])) {
        foreach ($this->shortcodes[$data['name']]["atts"] as $key => $value) {
          if ($value["required"] && isset($data['atts'][$key]) == false) {
            $errors[] = array(
              'message' => "The attribute " . $key . " is required.",
              'file' => $entity_path
            );
          }
          if ($value["required"] && isset($data['atts'][$key]) && (empty($data['atts'][$key]) || !$data['atts'][$key])) {
            $errors[] = array(
              'message' => "The attribute " . $key . " is empty.",
              'file' => $entity_path
            );
          }
          if (isset($data['atts'][$key]) && $value["required"] && isset($value['relation'])) {
            if ($value['relation_type'] == 'entity') {
              $relation = Entity::search_entity_path_from_id($data['atts'][$key]);
              if ($relation == false) {
                $errors[] = array(
                  'message' => "The entity linked to this shortcode was not found.",
                  'file' => $entity_path
                );
              }
            }
          }
        }
      }
    }

    return $errors;

  } //EOM

  public function get_shortcode_button_download($data)
  {

    $file_path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/downloads/config.json';
    $content = File::factory($file_path)->load();
    if (isset($data['atts']['category']) == false) {
      $content = false;
    } else {
      $content = array(
        'value' => $content['downloads'][$data['atts']['category']]
      );
      $content = Utils::apply_format_fields_downloads($content);
    }
    $data['downloads'] = $content;
    return $data;

  } //EOM

  public function get_shortcode_model($data, $entity_path)
  {

    if ($data['atts']['context-id']) {
      $entity_path = Entity::search_entity_path_from_id($data['atts']['context-id']);
    }
    $model = Model::factory($data['atts']['id'], $entity_path)->get_model();
    $data['content'] = $model['content'];
    foreach ($data['atts'] as $k => $v) {
      $variable = '{' . $k . '}';
      $data['content'] = str_replace($variable, $v, $data["content"]);
    }
    return $data;

  } //EOM

  public function get_shortcode_form_contact($data)
  {

    $action = Arr::get($_POST, 'action', false);
    $name = Arr::get($_POST, 'name', false);
    $email = Arr::get($_POST, 'email', false);
    $subject = Arr::get($_POST, 'subject', false);
    $message = Arr::get($_POST, 'message', false);
    $captcha = Arr::get($_POST, 'g-recaptcha-response', false);
    $errors = array();
    if ($action == 'form_contact') {
      $validation = Validation::factory($_POST);
      $validation->rule('name', 'not_empty');
      $validation->rule('email', 'not_empty');
      $validation->rule('email', 'email');
      $validation->rule('subject', 'not_empty');
      $validation->rule('message', 'not_empty');
      $validation->rule('g-recaptcha-response', 'not_empty');
      if ($validation->check()) {
        $secretKey = $this->config['captcha']['secret_key'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $args = array('secret' => $secretKey, 'response' => $captcha);
        $options = array(
          'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($args)
          )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        $responseKeys = json_decode($response, true);
        if ($responseKeys["success"]) {
          $html = '<div>From: ' . $name . '<' . $email . '></div>';
          $html .= '<div>Subject: ' . $subject . '</div>';
          $html .= '<div></div>';
          $html .= '<div>Message Body:</div>';
          $html .= $message;
          $html .= '<div></div>';
          $html .= '<div>--</div>';
          $html .= '<div>This email was sent from the Solarus contact form on https://www.solarus-games.org</div>';
          $mailer = Email::factory();
          $mailer
            ->to($data['atts']['to-email'], $data['atts']['to-name'])
            ->from($email, $name)
            ->reply_to($email, $name)
            ->subject($subject)
            ->html($html)
            ->send();
        }
        $data['errors'] = array();
      } else {
        $data['errors'] = $validation->errors();
      }
    }

    return $data;

  } //EOM

  public function get_shortcode_summary($data)
  {

    $context = Session::instance()->get('context');
    if ($context == false) {
      return false;
    }
    $entity = $context['entity'];
    $content = $entity['content']['value'];

    preg_match_all('/(#+ +.*)/', $content, $matches);

    $elements = $matches[1];
    $summary = array();
    $level_min = false;
    foreach ($elements as $element) {
      $level = substr_count($element, '#');
      if ($level_min == false) {
        $level_min = $level;
      } else if ($level < $level_min) {
        $level_min = $level;
      }
    }
    $levelsIndex = array();
    foreach ($elements as $element) {
      $level = substr_count($element, '#');
      $element = str_replace("#", "", $element);
      $summary[] = array(
        'label' => $element,
        'level' => $level - $level_min + 1,
        'index' => 0
      );
      $levelsIndex[$level - $level_min + 1] = 0;
    }
    foreach ($summary as $k => $v) {
      $summary[$k]['index'] = $levelsIndex[$v['level']];
      if ($data['atts']['level-min'] && $v['level'] < $data['atts']['level-min']) {
        unset($summary[$k]);
      }
      if ($data['atts']['level-max'] && $v['level'] > $data['atts']['level-max']) {
        unset($summary[$k]);
      }
      $levelsIndex[$v['level']]++;
    }
    $data['summary'] = $summary;

    return $data;

  } //EOM

  public function get_shortcode_image($data, $entity_path)
  {
    $config = Kohana::$config->load('config');
    $path = '/' . $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . dirname($entity_path) . '/' . $data['atts']['url'];
    $data['atts']['url'] = $path;
    
    return $data;

  } //EOM

  public function get_shortcode_search($data, $entity_path)
  {
    $path = $this->config['dirs']['data']  . '/' . I18n::lang() . '/config/search.json';
    $scopes = File::factory($path)->load();
    // Search
    $search = false;
    if ($data['atts']['search']) {
      $search = $data['atts']['search'];
    } elseif (Arr::get($_GET, 'search', false)) {
      $search = Arr::get($_GET, 'search', false);
    }
    // Scope
    $scope = false;
    if (isset($scopes[$data['atts']['scope']])) {
      $scope = $data['atts']['scope'];
    } elseif (Arr::get($_GET, 'scope', false)) {
      $scope = Arr::get($_GET, 'scope', false);
    }
    if ($scope && isset($scopes[$scope])) {
      $scope = $scopes[$scope];
    } else {
      $scope = false;
    }
    $data['scope'] = $scope;
    $data['search'] = $search;
    $items = array();
    foreach($data['scope']['entities'] as $kEntity => $vEntity) {
      $newEntities = Entity::search_entities_from_fields($vEntity, $search, $kEntity);
      if ($newEntities) {
        $items = array_merge($items, $newEntities);
      }
    }
    if ($scope['count'] > -1) {
      $nb = 0;
      $entities = array();
      foreach($items as $item) {
        if ($nb < $scope['count']) {
          $entities[] = $item;
        }
        $nb++;
      }
    } else {
      $entities = $items;
    }
    $score_name = '_score';
    usort($entities, function ($a, $b) use ($score_name) {
      return $a[$score_name] < $b[$score_name];
    });
    // Count
    $data['entities'] = $entities;

    return $data;

  } //EOM

} //EOC
