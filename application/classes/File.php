<?php

class File
{

  private $config;
  private $path;

  public function __construct($path = false)
  {

    $this->config = Kohana::$config->load('config');
    $this->path = $this->set_path($path);

  } //EOCo

  public static function factory($path = false)
  {
    return new File($path);

  } //EOM

  public function set_path($path = false)
  {
    if ($path == false) {
      return false;
    }
    $this->path = $path;

    return $this->path;

  } //EOM

  public function get_path()
  {

    return $this->path;

  } //EOM

  public function load()
  {
    if ($this->path == false) {
      return false;
    }
    $path = $this->path;
    if (is_file($path) == false) {
      return false;
    }
    $content = file_get_contents($path);
    if (Utils::get_file_extension($path) == 'json') {
      $content = json_decode($content, true);
    }
    return $content;

  } //EOM

  public function save($data = array()) {

    if ($this->path == false) {
      return false;
    }
    $path = $this->path;
    $json = $data;
    if (is_array($json)) {
      $json = json_encode($data);
    }
    file_put_contents($path, $json);

    return $data;

  } //EOM

} //EOC
