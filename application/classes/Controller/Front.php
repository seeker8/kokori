<?php

class Controller_Front extends Controller_Template
{

  public $template = "front/template";
  public $params = array();
  public $template_data = array();
  public $is_error = false;
  public $config;
  public $config_page;
  public $page_path;

  public function before()
  {

    parent::before();
    $this->config = Kohana::$config->load('config');
    $this->check_version();
    // Load Main config page
    $path = $this->config['dirs']['data'] . '/config.json';
    $this->config_page = File::factory($path)->load();
    $this->params['language'] = $this->request->param("language", $this->config_page['default_language']);
    $this->params['slug'] = $this->request->param("slug", false);
    // Check if redirect exist
    $slug = $this->params['language'];
    if ($this->params['slug']) {
      $slug = $slug . '/' . $this->params['slug'];
    }
    $redirect = URL::search_redirection_from_slug($slug);
    if ($redirect) {
      HTTP::redirect($redirect['slug'], isset($redirect['type']) ? $redirect['type'] : 301);
    }
    // Check if language exist
    if ('data' !== $this->params['language'] && !isset($this->config['languages'][$this->params['language']])) {
      $redirectUrl = $this->config['default_language'] . '/' . $this->params['language'];
      if ($this->params['slug']) {
        $redirectUrl = $redirectUrl . '/' . $this->params['slug'];
      }
      HTTP::redirect($redirectUrl, 301);
    }
    // Set Language
    if ($this->params['language']) {
      I18n::lang($this->params['language']);
    } else {
      I18n::lang($this->config['default_language']);
    }
    // Retrieve entity type and entity id
    if ($this->params['slug']) {
      $slug = $this->params['slug'];
      Session::instance()->set('slug', $slug);
      Session::instance()->set('slug_tmp', $slug);
      $this->page_path = Entity::search_page_path_from_slug($slug);
    } else {
      $this->page_path = "page/" . $this->config_page["default_path"];
      Session::instance()->set('slug', '/');
      Session::instance()->set('slug_tmp', '/');
    }
    // Forward 404 if language is not valid
    $languageFound = false;
    foreach($this->config_page['languages'] as $kLang => $vLang) {
      if ($kLang == I18n::lang()) {
        $languageFound = true;
      }
    }
    if ($languageFound == false) {
      I18n::lang($this->config['default_language']);
      $this->is_error = 404;
      $this->response->status(404);
    }
    // Forward 404 if entity is not valid
    if ($this->page_path == false) {
      $this->is_error = 404;
      $this->response->status(404);
    }
    // Cache
    if ($this->config['cache']) {
      $path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/config/cache.json';
      $configCache = File::factory($path)->load();
      if (!in_array($this->params['slug'], $configCache['disable'])) {
        $key = I18n::lang() . '/' . $this->params['slug'];
        $cache = Cache::instance('file');
        $output = $cache->get($key);
        if ($output) {
          die($output);
        }
      }
    }
    // Generate and check Context
    $context = $this->generate_context();
    if ($context == false || $context && $context['entity']['publish']['value'] == false) {
      $this->is_error = 404;
      $this->response->status(404);
    } else {
      $entry = $this->generate_entry();
      $this->template_data['entry'] = $entry;
    }
    $this->template_data['config'] = $this->config_page;
    // Theme
    $this->template_data['theme'] = false;
    if (isset($this->config_page["theme"]) && $this->config_page["theme"]) {
      $this->template_data['theme'] = $this->config_page["theme"];
    }
    // Meta title
    $this->template_data['meta_title'] = false;
    if (isset($this->config_page["meta_title"]) && $this->config_page["meta_title"]) {
      $this->template_data['meta_title'] = $this->config_page["meta_title"];
    }
    if (isset($entry) && isset($entry["entity"]["meta_title"]["value"]) && $entry["entity"]["meta_title"]["value"]) {
      if ($this->template_data['meta_title']) {
        if (isset($this->config_page["meta_title_separator"]) && $this->config_page["meta_title_separator"]) {
          $this->template_data['meta_title'].= $this->config_page["meta_title_separator"];
        }
        $this->template_data['meta_title'].= $entry["entity"]["meta_title"]["value"];
      } else {
        $this->template_data['meta_title'] = $entry["entity"]["meta_title"]["value"];
      }
    }
    // Meta description
    $this->template_data['meta_description'] = false;
    if (isset($entry) && isset($entry["entity"]["meta_description"]["value"]) && $entry["entity"]["meta_description"]["value"]) {
      $this->template_data['meta_description'] = $entry["entity"]["meta_description"]["value"];
    }
    // Meta image
    $this->template_data['meta_image'] = false;
    if (isset($entry) && isset($entry["entity"]["meta_image"]["value"]) && $entry["entity"]["meta_image"]["value"]) {
      $this->template_data['meta_image'] = $entry["entity"]["meta_image"]["value"];
    }
    // Meta robots
    $this->template_data['meta_robots'] = false;
    if (isset($this->config_page["active_engine"]) && $this->config_page["active_engine"]) {
      if (isset($this->config_page["meta_robots"]) && $this->config_page["meta_robots"]) {
        $this->template_data['meta_robots'] = $this->config_page["meta_robots"];
      }
      if (isset($entry) && isset($entry["entity"]["meta_robots"]["value"]) && $entry["entity"]["meta_robots"]["value"]) {
        $this->template_data['meta_robots'] = $entry["entity"]["meta_robots"]["value"];
      }
    } else {
      $this->template_data['meta_robots'] = "noindex, nofollow";
    }

  } //EOM

  public function after()
  {

    if ($this->is_error == 404) {
      $path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/errors/404.md';
      $content = File::factory($path)->load();
      $content = Utils::convert_content($content);
      $content = View::factory('front/errors/404', array('content' => $content));
    } else {
      $content = View::factory('front/single', array('data' => $this->template_data));
    }
    $this->template->content = $content;
    $this->template->data = $this->template_data;
    if ($this->config['cache']) {
      $path = $this->config['dirs']['data'] . '/' . I18n::lang() . '/config/cache.json';
      $configCache = File::factory($path)->load();
      if (!in_array($this->params['slug'], $configCache['disable'])) {
        $key = I18n::lang() . '/' . $this->params['slug'];
        $cache = Cache::instance('file');
        $cache->set($key, $this->template->render());
      }
    }

    parent::after();

  } //EOM

  private function generate_context()
  {
    $context = array(
      'entity' => Entity::factory($this->page_path)->get_entity(false)
    );
    $context['page_path'] = $this->page_path;
    $context['language'] = $this->params['language'];
    Session::instance()->set('context', $context);

    return $context;

  } //EOM

  private function generate_entry()
  {

    $entity = Entity::factory($this->page_path)->get_entity();
    $entry = array(
      'entity' => $entity,
    );

    return $entry;

  } //EOM

  private function check_version()
  {
    $dataPath = $this->config['dirs']['data']  . '/config/version.json';
    $dataVersion = File::factory($dataPath)->load();
    $writablePath = $this->config['dirs']['writable']  . '/version.json';
    $writableVersion = File::factory($writablePath)->load();
    if (!$dataVersion || !isset($dataVersion['version'])) {
      return false;
    }
    if (!$writableVersion || !isset($writableVersion['version']) || $dataVersion['version'] != $writableVersion['version']) {
      // Flush cache
      Cache::instance('file')->delete_all();
      // Flush all indexes
      Index::delete_all();
      // Write new version
      File::factory($writablePath)->save($dataVersion);
    }

  } //EOM

  public function action_request()
  {

  } //EOM

} //EOC
