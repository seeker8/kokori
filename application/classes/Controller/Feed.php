<?php

class Controller_Feed extends Controller_Template
{

  public $template = "feed/template";
  public $params = array();
  public $template_data = array();
  public $action = false;
  public $config;

  public function before()
  {

    parent::before();
    $this->config = Kohana::$config->load('config');
    // Load Main config page
    $path = $this->config['dirs']['data'] . '/config.json';
    $this->config_page = File::factory($path)->load();
    $this->params['language'] = $this->request->param("language", $this->config_page['default_language']);
    $this->params['slug'] = $this->request->param("slug", false);
    // Check if language exist
    if ('data' !== $this->params['language'] && !isset($this->config['languages'][$this->params['language']])) {
      $redirectUrl = $this->config['default_language'] . '/' . $this->params['language'];
      if ($this->params['slug']) {
        $redirectUrl = $redirectUrl . '/' . $this->params['slug'];
      }
      HTTP::redirect($redirectUrl, 301);
    }
    // Set Language
    if ($this->params['language']) {
      I18n::lang($this->params['language']);
    } else {
      I18n::lang($this->config['default_language']);
    }
    // Load Feeds
    $path = $this->config['dirs']['data']  . '/' . I18n::lang() . '/config/feeds.json';
    $feeds = File::factory($path)->load();
    $foundFeed = false;
    foreach($feeds as $feed) {
      if ($feed['slug'] == $this->params['slug']) {
        $foundFeed = $feed;
      }
    }
    if ($foundFeed) {
      $this->template_data['title'] = $foundFeed['title'];
      $this->template_data['description'] = $foundFeed['description'];
      $this->template_data['link'] = URL::current();
      $listing = Entity::get_listing($foundFeed['entity-type'], $foundFeed['count'], $foundFeed['order-by'], $foundFeed['order']);
      if ($listing) {
        $this->template_data['items'] = array();
        foreach($listing as $currentItem) {
          $this->template_data['items'][] = array(
            'title' => $currentItem[$foundFeed['key-title']]['value'],
            'description' => $currentItem[$foundFeed['key-description']]['value'],
            'link' => Url::get($currentItem['page_override']['value']['slug'])
          );
        }
      }
    } else {
      HTTP::redirect('/', 301);
    }

  } //EOM

  public function after()
  {

    $this->template->data = $this->template_data;
    $this->response->headers('Content-Type','text/xml');

    parent::after();

  } //EOM

  public function action_request()
  {

  } //EOM

} //EOC
