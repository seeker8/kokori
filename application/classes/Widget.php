<?php

class Widget
{

  private $config;
  private $widget;

  public function __construct($widget = false)
  {

    $this->config = Kohana::$config->load('config');
    $this->widget = $widget;

  } //EOCo

  public static function factory($widget = false)
  {
    return new Widget($widget);

  } //EOM

  public function get_widget($data = array()) {

    if ($this->widget == false) {
      return false;
    }
    $method = 'get_widget_data_' . $this->widget;
    if (method_exists($this, $method)) {
      $data = $this->{$method}($data);
      return View::factory('front/widgets/' . $this->widget, array('data' => $data));
    }

    return false;

  } //EOM

  public function get_widget_data_breadcrumb($slug = false) {

    if ($slug == false) {
      return false;
    }
    $relationship = Url::search_relationship_from_slug($slug);
    $data["breadcrumb"] = array();
    $nb = 0;
    foreach($relationship as $k => $v) {
      $nb++;
      $slug = false;
      if ($nb != count($relationship)) {
        $slug = $v["slug"]["value"];
      }
      $data["breadcrumb"][] = array(
        'label' => $v["title"]["value"],
        'title' => $v["title"]["value"],
        'slug' => $slug
      );
    }

    return $data;

  } //EOM

  public function get_widget_data_languages($data = array()) {

    if (isset($data['languages']) == false) {
      return false;
    }
    if (count($data['languages']) <= 1) {
      return false;
    }
    $context = Session::instance()->get('context');
    $slug = $context['entity']['slug']['value'];
    foreach($data['languages'] as $k => $v) {
      $entity = Entity::search_page_path_from_slug($slug, $k);
      $data['languages'][$k]['url'] = '/' . $k;
      if ($entity) {
        $data['languages'][$k]['url'] = '/' . $k . '/' . $slug;
      }
    }
    $data['language'] = I18n::lang();

    return $data;

  } //EOM

  public function get_widget_data_menu($data = array()) {

    if (isset($data['id']) == false) {
      return false;
    }
    if (isset($data['slug']) == false) {
      return false;
    }
    $relationship = Url::search_relationship_from_slug($data['slug']);
    $path = $this->config['dirs']['data']  . '/' . I18n::lang() . '/config/menus.json';
    $menus = File::factory($path)->load();
    if (isset($menus[$data['id']]) == false || $menus[$data['id']] == false) {
      $data['menu'] = false;
    } else {
      $menu = $menus[$data['id']];
      if (count($menu['elements']) > 0) {
        foreach($menu['elements'] as $k => $v) {
          if (Utils::is_absolute_url($v['url']) == false) {
            $selected = false;
            if (is_array($relationship)) {
              foreach( $relationship as $kr => $vr) {
                if ($vr['slug'] && $v['url'] == $vr['slug']['value']) {
                  $selected = true;
                }
              }
            }
            $menu['elements'][$k]['selected'] = $selected;
          }
        }
      }
      $data['menu'] = $menu;
    }

    return $data;

  } //EOM

  public function get_widget_data_content($data = array()) {

    if (isset($data['id']) == false) {
      return false;
    }
    $data['output'] = false;
    $path = $this->config['dirs']['data']  . '/' . I18n::lang() . '/widgets/content/' . $data['id'] . '.md';
    if (is_file($path)) {
      $output = File::factory($path)->load();
      $path = '../widgets/content/' . $data['id'] . '.md';
      $output = Utils::convert_content($output, $path);
      $data['output'] = $output;
    }

    return $data;

  } //EOM

  public function get_widget_data_search($data = array()) {

    if (isset($data['scope']) == false) {
      return false;
    }
    $path = $this->config['dirs']['data']  . '/' . I18n::lang() . '/config/search.json';
    $scopes = File::factory($path)->load();
    if (isset($scopes[$data['scope']])) {
      $scope = $data['scope'];
      $data['scope'] = $scopes[$data['scope']];
      $data['scope']['scope'] = $scope;
    } else {
      $data['scope'] = false;
    }

    return $data;

  } //EOM

} //EOC
