<?php
class URL extends Kohana_URL
{

  public static function search_redirection_from_slug($slug = false) {

    if ($slug == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data']  . '/config/redirects.json';
    $redirects = File::factory($path)->load();
    if (isset($redirects[$slug])) {
      $redirect = $redirects[$slug];
      return  $redirect;
    }

    return false;

  } //EOM

  public static function search_relationship_from_slug($slug = false) {

    if ($slug == false) {
      return false;
    }
    $context = Session::instance()->get('context');
    $entity = $context["entity"];
    $elements = array();
    $elements[] = $entity;
    while(isset($entity["parent"]) && $entity["parent"]["value"]) {
      $entityPath = Entity::search_page_path_from_slug($entity["parent"]["value"]);
      if ($entityPath) {
        Session::instance()->set('slug_tmp', $entity['parent']['value']);
        $entity = Entity::factory($entityPath)->get_entity();
        if ($entity) {
          $elements[] = $entity;
        }
      }
    }
    krsort($elements);

    return $elements;

  } //EOM

  public static function current() {

    return URL::site(Request::detect_uri(), TRUE).URL::query();

  } //EOM

  public static function get($uri = false) {

    if (!$uri) {
      return false;
    }
    return Url::site(I18n::lang() . '/' . $uri, TRUE);

  } //EOM

} //EOC
