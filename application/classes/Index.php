<?php

class Index
{

  private $config;
  private $path;

  public function __construct($path = false)
  {

    $this->config = Kohana::$config->load('config');
    $this->path = $this->set_path($path);

  } //EOCo

  public static function factory($path = false)
  {
    return new Index($path);

  } //EOM

  public function set_path($path = false)
  {
    if ($path == false) {
      return false;
    }
    $this->path = $path;

    return $this->path;

  } //EOM

  public function load()
  {
    if ($this->path == false) {
      return false;
    }
    $path = $this->path;
    if (is_file($path) == false) {
      return false;
    }
    $content = file_get_contents($path);
    if (Utils::get_file_extension($path) == 'json') {
      $content = json_decode($content, true);
    }
    return $content;

  } //EOM

  public static function generate_index($fields = array(), $search = false, $type = 'page') {

    if (!$search) {
      return false;
    }
    if (count($fields) == 0) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $key = I18n::lang() . '-' . $type . '-' . json_encode($fields);
    $key = sha1($key);
    $indexPath = $config['dirs']['writable']  . '/search/' . $key . '.json';
    $index = Index::factory($indexPath)->load();
    // Generate index
    if (!$index) {
      $index = array();
      $path = $config['dirs']['data'] . '/' . I18n::lang() . '/entities/' . $type;
      $files = Dir::factory($path)->load();
      foreach($files as $file) {
        if ('structure.json' == basename($file) ){
          continue;
        }
        $dataEntity = File::factory($file)->load();
        $entity_path = Entity::search_entity_path_from_filename($file);
        $entity = Entity::factory($entity_path)->get_entity(false);
        $row = array();
        foreach($fields['output'] as $keyOutput => $valueOutput) {
          if ($valueOutput && isset($entity[$valueOutput]['value'])) {
            $row[$keyOutput] = $entity[$valueOutput]['value'];
          } else {
            $row[$keyOutput] = false;
          }
        }
        $row['url'] = false;
        if(isset($entity['page_slug']['value'])) {
          $row['url'] = $entity['page_slug']['value'];
        } elseif (isset($entity['slug']['value'])) {
          $row['url'] = $entity['slug']['value'];
        }
        if ($row['url']) {
          $index[$row['url']] = $row;
        }
      }
      // Save index
      Index::factory($indexPath)->save($index);
    }

    return $index;

  } //EOM

  public static function search_in_index($fields = array(), $search = false, $type = 'page') {

    if (!$search) {
      return false;
    }
    if (count($fields) == 0) {
      return false;
    }
    $entities = array();
    $config = Kohana::$config->load('config');
    $key = I18n::lang() . '-' . $type . '-' . json_encode($fields);
    $key = sha1($key);
    $indexPath = $config['dirs']['writable']  . '/search/' . $key . '.json';
    $index = Index::factory($indexPath)->load();
    if (!$index) {
      return false;
    }
    $score_name = '_score';
    
    // Generate index
    foreach ($index as $entity) {
      $score = 0;

      foreach ($fields['criteria'] as $kField => $field) {
        if (isset($entity[$kField])) {
          $field_weight = 1;
          if ($field['weight']) {
            $field_weight = intval($field['weight']);
          }

          switch ($field['comparison']) {
            case 'like':
              if (strpos(strtolower($entity[$kField]), strtolower($search)) !== false) {
                $score += $field_weight;
              }
              break;
            case '==':
              if (strtolower($entity[$kField]) == strtolower($search)) {
                $score += $field_weight;
              }
              break;
            default:
              break;
          }
        }
      }

      if ($score > 0) {
        $entity[$score_name] = $score;
        $entities[] = $entity;
      }
    }

    return $entities;

  } //EOM

  public function save($data = array()) {

    if ($this->path == false) {
      return false;
    }
    $path = $this->path;
    $json = $data;
    if (is_array($json)) {
      $json = json_encode($data);
    }

    $dir = dirname($path);
    if (!is_dir($dir)) {
      mkdir($dir);
    }

    file_put_contents($path, $json);

    return $data;

  } //EOM

  public static function delete_all() {

    $config = Kohana::$config->load('config');
    $directory = $config['dirs']['writable']  . '/search';
    if (is_dir($directory)) {
      $dir = opendir($directory);
      while ($file = readdir($dir))  {
        if ($file != "." && $file != "..")
          unlink($directory . '/' . $file);
        }
        closedir($dir);
    }

  } //EOM

} //EOC
