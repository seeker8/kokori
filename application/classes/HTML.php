<?php
class HTML extends Kohana_HTML
{

  public static function style($file, array $attributes = NULL, $protocol = NULL, $index = FALSE) {

    $file = "themes/solarus/" . $file;
    return parent::style($file, $attributes, $protocol, $index);

  }

  public static function script($file, array $attributes = NULL, $protocol = NULL, $index = FALSE) {

    $file = "themes/solarus/" . $file;

    return parent::script($file, $attributes, $protocol, $index);

  }

  public static function get_list_from_array($data = array())
  {

    $html = '<ul>';
    foreach ($data as $key => $value) {
      $function = is_array($value) ? __FUNCTION__ : 'htmlspecialchars';
      $html .= '<li><b>' . $key . ':</b> <i>' . $function($value) . '</i></li>';
    }
    return $html . '</ul>';

  } //EOM

  public static function get_logo() {

    return HTML::get_theme_image('logo.svg');

  } //EOM

  public static function get_theme_image($name = false) {

    if ($name == false) {
      return false;
    }
    $config = Kohana::$config->load('config');
    $path = $config['dirs']['data'] . '/config.json';
    $config_page = File::factory($path)->load();
    $logo = "/themes/" .$config_page["theme"] . "/assets/img/" . $name;


    return '<img src="' . $logo . '"/>';

  } //EOM

  public static function get_breadcrumb($breadcrumb = false) {

    if ($breadcrumb == false) {
      return false;
    }
    if (is_array($breadcrumb) == false) {
      return false;
    }
    $html = '<nav>';
    $html .= '<ol class="breadcrumb">';
    foreach($breadcrumb as $k => $v) {
      if ($v["slug"]) {
        $url =  $v['slug'];
        if (strpos($url, 'http://') !== 0 && strpos($url, 'https://') !== 0) {
          $url = '/' . I18n::lang() . '/' .$url;
        }
        $html .= '<li class="breadcrumb-item">';
        $html .= '<a href="' . $url . '">' . $v['label']. '</a>';
        $html .= '</li>';
      } else {
        $html .= '<li class="breadcrumb-item active">' . $v['label']. '</li>';
      }
    }
    $html .= '</ol>';
    $html .= '</nav>';

    return $html;

  } //EOM

  public static function get_menu($id, $menu, $children = false, $show_mobile = true) {

    if ($children == false) {
      $elements = $menu['elements'];
      $effects = '';
      if (isset($menu['effect-in']) && $menu['effect-in']) {
        $effects .= ' data-effect-in="' . $menu['effect-in'] . '"';
      }
      $html = '<nav id="menu-' . $id .'" data-toggle="menu" class="navbar navbar-expand-lg d-none d-lg-block"' . $effects .'>';
      $html .= '<ul class="navbar-nav">';
    } else {
      $show_mobile_class = 'd-block';
      if ($show_mobile == false) {
        $show_mobile_class = ' invisible-mobile';
      }
      $elements = $menu;
      $html = '<div class="navbar-nav navbar-submenu animated' . $show_mobile_class . '"><ul>';
    }
    foreach ($elements as $key => $value) {
      if ($value['icon'] == false && $value['label'] == false) {
        continue;
      }
      $url =  $value['url'];
      if ($url && strpos($url, 'http://') !== 0 && strpos($url, 'https://') !== 0) {
        $url = '/' . I18n::lang() . '/' .$url;
      }
      $selected = false;
      if (isset($value["selected"]) && $value["selected"]) {
        $selected = ' selected';
      }
      $html .= '<li class="nav-item">';
      $html .= '<a target="' . $value["target"]. '" class="nav-link' . $selected;
      $html .= '"';
      if ($url) {
        $html .= ' href="' . $url . '"';
      }
      $html .= '>';
      if ($value['icon']) {
        $html .= '<i class="fa fa-' . $value['icon'] .'"></i>';
      }
      if ($value['label']) {
        $html .= $value['label'];
      }
      $html .= '</a>';
      if (is_array($value['submenu'])) {
        $html .= HTML::get_menu($id, $value['submenu']['elements'], true, $value['submenu']['show_mobile']);
      }
      $html .= '</li>';
    }
    if ($children == false) {
      $html . '</div></ul></nav>';
    } else {
      $html .= '</ul>';
    }
    return $html;

  } //EOM

  public static function get_summary($entities = array(), $is_started = false) {

    $active = '';
    if ($entities['active']) {
      $active = ' class="active"';
    }
    $html = '';
    if ($is_started == false) {
      $html.='<ul>';
    }
    $html.= '<li' . $active .'>';
    $html.= '<a title="' . $entities['entity']['title']['value'] . '" href="' . $entities['entity']['page_slug']['value'] . '">';
    $html.= $entities['entity']['title']['value'];
    $html.= '</a>';
    if (count($entities['childrens']) > 0) {
      $html.= '<ul>';
      foreach($entities['childrens'] as $k => $v) {
        $html .= HTML::get_summary($v, true);
      }
      $html.='</ul>';
    }
    $html.= '</li>';
    if ($is_started == false) {
      $html.='</ul>';
    }

    return $html;

  } //EOM

} //EOC
