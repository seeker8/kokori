<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">
  <channel>
    <title><?php echo $data['title'];?></title>
    <link><?php echo $data['link'];?></link>
    <description><?php echo $data['description'];?></description>
    <?php foreach($data['items'] as $item):?>
      <item>
        <title><![CDATA[<?php echo $item['title'];?>]]></title>
        <link><![CDATA[<?php echo $item['link'];?>]]></link>
        <description><![CDATA[<?php echo $item['description'];?>]]></description>
      </item>
      <?php endforeach;?>
  </channel>
</rss>