<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <?php if ($data["meta_title"]):?>
    <title><?php echo $data["meta_title"];?></title>
    <?php endif;?>
    <?php if ($data["meta_description"]):?>
    <meta name="description" content="<?php echo $data["meta_description"];?>"/>
    <?php endif;?>
    <?php if ($data["meta_robots"]):?>
    <meta name="robots" content="<?php echo $data["meta_robots"];?>"/>
    <?php endif;?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/solarus/assets/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/solarus/assets/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/solarus/assets/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/solarus/assets/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/solarus/assets/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/solarus/assets/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/solarus/assets/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/solarus/assets/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/solarus/assets/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="512x512"  href="/themes/solarus/assets/icons/favicon-512x512.png">
    <link rel="icon" type="image/png" sizes="256x256"  href="/themes/solarus/assets/icons/favicon-256x256.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/solarus/assets/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="144x144"  href="/themes/solarus/assets/icons/android-icon-144x144.png">
    <link rel="icon" type="image/png" sizes="128x128"  href="/themes/solarus/assets/icons/favicon-128x128.png">
    <link rel="icon" type="image/png" sizes="96x96"  href="/themes/solarus/assets/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="72x72"  href="/themes/solarus/assets/icons/android-icon-72x72.png">
    <link rel="icon" type="image/png" sizes="64x64"  href="/themes/solarus/assets/icons/favicon-64x64.png">
    <link rel="icon" type="image/png" sizes="48x48" href="/themes/solarus/assets/icons/favicon-48x48.png">
    <link rel="icon" type="image/png" sizes="36x36" href="/themes/solarus/assets/icons/android-icon-36x36.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/solarus/assets/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/solarus/assets/icons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffbc00">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffbc00">
    <!-- Open Graph protocol -->
    <?php if ($data["meta_title"]):?>
    <meta property="og:title" content="<?php echo $data["meta_title"];?>"/>
    <?php endif;?>
    <?php if ($data["meta_image"]):?>    
    <meta property="og:image" content="<?php echo $data["meta_image"];?>"/>
    <?php endif;?>
    <?php if ($data["meta_description"]):?>    
    <meta property="og:description" content="<?php echo $data["meta_description"];?>"/>
    <?php endif;?>
    <meta property="og:url" content="<?php echo URL::current();?>"/>
    <meta property="og:type" content="website"/>
    <?php $language_list = $data['config']['languages'];?>
    <?php $current_lang = $language_list[I18n::lang()];?>
    <?php if (array_key_exists('locale_tag', $current_lang)):?>
      <?php $current_locale_tag = $current_lang['locale_tag'];?>
      <meta property="og:locale" content="<?php echo $current_locale_tag?>"/>
      <?php foreach($language_list as $language):?>
        <?php if (array_key_exists('locale_tag', $language)):?>
          <?php $locale_tag = $language['locale_tag'];?>
          <?php if ($locale_tag != $current_locale_tag):?>
            <meta property="og:locale-alternate" content="<?php echo $locale_tag;?>"/>
          <?php endif;?>
        <?php endif;?>        
      <?php endforeach;?>
    <?php endif;?>
    <?php echo HTML::style('assets/plugins/bootstrap/css/bootstrap.min.css');?>
    <?php echo HTML::style('assets/plugins/animate/animate.css');?>
    <?php echo HTML::style('assets/plugins/prism/prism.css');?>
    <?php echo HTML::style('assets/plugins/owl/dist/assets/owl.carousel.min.css');?>
    <?php echo HTML::style('assets/plugins/fancybox/dist/jquery.fancybox.min.css');?>
    <?php echo HTML::style('assets/css/front.css');?>
    <?php echo HTML::style('assets/css/front-mobile.css');?>
    <?php echo HTML::script('assets/plugins/jquery/jquery.min.js');?>
  </head>
  <body>
    <div class="container-full">
      <?php echo View::factory('front/header', array('data' => $data));?>
      <?php if (isset($data['entry']) && $data['entry']['entity']['has_breadcrumb']['value']):?>
        <?php echo View::factory('front/breadcrumb', array('data' => $data));?>
      <?php endif;?>
      <?php echo $content;?>
      <?php echo View::factory('front/footer', array('data' => $data));?>
    </div>
    <?php echo HTML::script('assets/plugins/popper/popper.min.js');?>
    <?php echo HTML::script('assets/plugins/bootstrap/js/bootstrap.min.js');?>
    <?php echo HTML::script('assets/plugins/fontawesome/js/all.js');?>
    <?php echo HTML::script('assets/plugins/appear/appear.js');?>
    <?php echo HTML::script('assets/plugins/owl/dist/owl.carousel.min.js');?>
    <?php echo HTML::script('assets/plugins/fancybox/dist/jquery.fancybox.min.js');?>
    <?php echo HTML::script('assets/plugins/paroller/dist/jquery.paroller.min.js');?>
    <?php echo HTML::script('assets/plugins/isotope/isotope.min.js');?>
    <?php echo HTML::script('assets/plugins/prism/prism.js');?>
    <?php echo HTML::script('assets/plugins/waitforimages/jquery.waitforimages.min.js');?>
    <?php echo HTML::script('assets/js/front.js');?>
  </body>
</html>
