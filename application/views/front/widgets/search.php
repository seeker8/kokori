<div class="widget widget-search">
  <form method="GET" action="/<?php echo I18n::lang() . '/' . $data['scope']['form-action'];?>?search=">
    <input type="hidden" name="scope" value="<?php echo $data['scope']['scope'];?>">
    <div class="input-group">
      <input class="form-control" name="search" placeholder="<?php echo $data['scope']['form-placeholder'];?>" type="text" value="<?php echo Arr::get($_GET, 'search', false);?>"/>
      <div class="input-group-append">
        <button class="btn btn-primary" title="<?php echo $data['scope']['form-button-text'];?>">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>
</div>