<div class="modal-header">
  <h2 class="modal-title"><?php echo $title;?></h2>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <?php if (empty($entity["image_thumbnail"]["value"]) == false):?>
    <div class="thumbnail">
      <img alt="<?php echo $entity["title"]["value"];?>" src="<?php echo $entity["image_thumbnail"]["value"];?>"/>
    </div>
  <?php endif;?>
  <div class="metas">
    <span><?php echo $download['version'];?></span>
    <?php if ($download['platform'] != 'any'):?>
      <span>
        <?php Utils::print_platform_icon_html($download['platform']);?>
        <?php echo $download['platform'];?></span>
    <?php endif;?>
    <?php if (isset($download['size']) && $download['size'] != -1 && $download['size'] != ''):?>
      <span><?php echo $download['size'];?></span>
    <?php endif;?>
  </div>
  <div class="content">
    <?php echo $download['content'];?>
  </div>
</div>
<?php if (!empty($download['file'])):?>
  <div class="modal-footer">
    <a target="_blank" href="<?php echo $download['file'];?>" download="<?php echo basename($download['file']);?>" class="btn btn-primary">
      <i class="fa fa-download"></i>
      <?php echo $buttonLabel;?>
    </a>
  </div>
<?php endif;?>