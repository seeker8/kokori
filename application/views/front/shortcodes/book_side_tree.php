<?php $childrens = false;?>
<?php foreach ($data['tree'] as $v): ?>
  <?php if (count($v['childrens']) > 0):?>
    <?php $childrens = true;?>
  <?php endif;?>
<?php endforeach; ?>
<div data-toggle="side-tree" class="shortcode shortcode-book-side-tree<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?><?php if (!$childrens):?> singleton<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <?php foreach ($data['tree'] as $v): ?>
    <?php echo HTML::get_summary($v);?>
  <?php endforeach; ?>
</div>