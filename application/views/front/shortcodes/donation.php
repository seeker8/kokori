<div class="shortcode shortcode-donation<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
  <h3>
    <?php if ($data['atts']['icon']):?>
      <?php echo $data['atts']['icon'];?>
    <?php endif;?>
    <?php echo $data['atts']['title'];?>
  </h3>
  <p>
    <?php echo $data['content'];?>
  </p>
  <div class="actions">
    <a href="<?php echo $data['atts']['link'];?>" target="_blank" title="<?php echo $data['atts']['title'];?>" class="btn btn-<?php echo $data['atts']['type'];?>">
      <?php echo $data['atts']['label'];?>
    </a>
  </div>
</div>