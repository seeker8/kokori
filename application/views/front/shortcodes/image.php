<div class="shortcode shortcode-image<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <?php $image_url = $data['atts']['url'];?>
  <?php if ($data['atts']['modal']):?>
  <a class="fancybox-img" href="<?php echo $image_url?>">
    <img src="<?php echo $image_url?>" alt="<?php echo $data['atts']['alt']?>"/>
  </a>
  <?php else:?>
    <img src="<?php echo $image_url?>" alt="<?php echo $data['atts']['alt']?>"/>
  <?php endif; ?>
</div>
