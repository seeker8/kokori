<?php if ($data["entity"]['medias']['value']):?>
  <div class="shortcode shortcode-game-medias<?php if (count($data["entity"]['medias']['value']) <= $data['atts']['nb-items-visible']):?> without-nav<?php endif;?> <?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
    <?php if ($data['atts']['title']):?>
      <h2><?php echo $data['atts']['title'];?></h2>
    <?php endif;?>
    <?php if ($data["entity"]['medias']['value']):?>
      <div class="owl-carousel<?php if (count($data["entity"]['medias']['value']) <= $data['atts']['nb-items-visible']):?> owl-carousel-without-nav<?php endif;?>" data-nb-items-visible="<?php echo $data['atts']['nb-items-visible'];?>">
        <?php foreach($data["entity"]['medias']['value'] as $media):?>
           <a class="item item-<?php echo $media["type"];?> fancybox-gallery" data-fancybox="gallery" href="<?php echo $media['url'];?>" title="<?php echo $data["entity"]["title"]["value"];?>">
             <img class="img-fluid" alt="<?php echo $data["entity"]["title"]["value"];?>" src="<?php echo $media['thumbnail'];?>"/>
             <?php if ($media["type"] == "youtube"):?>
                <i class="fa fa-film"></i>
             <?php endif;?>
           </a>
        <?php endforeach;?>
      </div>
    <?php endif;?>
  </div>
<?php endif;?>