<?php 
$data_atts = $data['atts'];
$effect_in = $data_atts['effect-in'];
$all_downloads = $data['downloads']['others'];
$has_multiple_downloads = count($all_downloads) > 1;
$default_download = $data['downloads']['default'];
?>

<div class="shortcode shortcode-inline shortcode-button-download<?php if ($effect_in):?> effect-in<?php endif;?>"<?php if ($effect_in):?>data-effect-in="<?php echo $effect_in;?>"<?php endif;?>>
  <button <?php if ($data['downloads']):?>
    data-toggle="download"
    data-file="default"
    data-id="downloads"
    data-category="<?php echo $data_atts['category'];?>"<?php endif;?>
    data-modal-title="<?php echo $data_atts['modal-title'];?>"
    data-modal-button-label="<?php echo $data_atts['modal-button-label'];?>"
    data-language="<?php echo I18n::lang();?>"
    type="button"
    class="btn<?php if ($data_atts['outline']):?> btn-outline-<?php echo $data_atts['type'];?><?php else:?> btn-<?php echo $data_atts['type'];?><?php endif;?><?php if($data_atts['size']):?> btn-<?php echo $data_atts['size'];?><?php endif;?><?php if ($has_multiple_downloads):?> with-dropdown<?php endif;?>"
    aria-haspopup="true"
    aria-expanded="false">
    <div class="icon">
      <?php if ($data_atts['icon']):?>
        <?php echo $data_atts['icon'];?>
      <?php endif;?>
    </div>
    <div class="label">
      <?php echo $data_atts['label'];?>
      <?php if (isset($all_downloads) && count($all_downloads) > 0):?>
      <small>
        <?php echo $default_download['version'];?> - <?php Utils::print_platform_icon_html($default_download['platform']);?><?php echo $default_download['platform'];?>
      </small>
      <?php endif;?>
    </div>
  </button>

  <?php if ($has_multiple_downloads):?>
    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
    <div class="dropdown-menu">
      <?php foreach($all_downloads as $k => $v):?>
        <a
          data-toggle="download" 
          data-file="<?php echo $k;?>"
          data-id="downloads"
          data-category="<?php echo $data_atts['category'];?>"
          data-modal-title="<?php echo $data_atts['modal-title'];?>"
          data-modal-button-label="<?php echo $data_atts['modal-button-label'];?>"
          data-language="<?php echo I18n::lang();?>"
          class="dropdown-item"
          href="#">
          <?php Utils::print_platform_icon_html($v['platform']);?>
          <?php echo $v["label"];?>
        </a>
      <?php endforeach;?>
    </div>
  <?php endif;?>
</div>
