<?php $childrens = false;?>
<?php foreach ($data['tree'] as $v): ?>
  <?php if (count($v['childrens']) > 0):?>
    <?php $childrens = true;?>
  <?php endif;?>
<?php endforeach; ?>
<div class="shortcode shortcode-book-chapter-listing<?php if ($data['atts']['effect-in']): ?> effect-in<?php endif; ?><?php if (!$childrens):?> singleton<?php endif;?>"<?php if ($data['atts']['effect-in']): ?> data-effect-in="<?php echo $data['atts']['effect-in']; ?>"<?php endif; ?>>
  <?php if ($childrens):?>
    <?php foreach ($data['tree'] as $v): ?>
      <h2><?php echo $v['entity']['title']['value']; ?></h2>
      <p><?php echo $v['entity']['excerpt']['value']; ?></p>
      <?php if (count($v['childrens']) > 0): ?>
          <?php foreach ($v['childrens'] as $ks => $vs): ?>
              <a href="<?php echo $vs['entity']["page_slug"]["value"]; ?>" title="<?php echo $vs['entity']["title"]["value"]; ?>">
                <div class="text">
                  <h3 class="title"><?php echo $vs['entity']["title"]["value"]; ?></h3>
                  <span class="metas">
                    <p class="excerpt"><?php echo $vs['entity']["excerpt"]["value"]; ?></p>
                  </span>
                </div>
                <i class="fa fa-chevron-right arrow-icon"></i>
              </a>
          <?php endforeach; ?>
      <?php endif; ?>
    <?php endforeach; ?>
  <?php else:?>
    <?php foreach ($data['tree'] as $v): ?>
      <a href="<?php echo $v['entity']["page_slug"]["value"]; ?>" title="<?php echo $v['entity']["title"]["value"]; ?>">
        <div class="text">
          <h3 class="title"><?php echo $v['entity']["title"]["value"]; ?></h3>
          <span class="metas">
                    <p class="excerpt"><?php echo $v['entity']["excerpt"]["value"]; ?></p>
                  </span>
        </div>
        <i class="fa fa-chevron-right arrow-icon"></i>
      </a>
    <?php endforeach;?>
  <?php endif;?>
</div>
