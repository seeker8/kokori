<div class="shortcode shortcode-book-chapter-navigation<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <?php if ($data['pagination']['previous']):?>
    <a href="/<?php echo I18n::lang() . '/' . $data['pagination']['previous']['page_override']['value']['slug'];?>" class="btn btn-default navigation-previous">
      <i class="fa fa-chevron-left arrow-icon"></i>
      <div class="text-lines">
        <div class="line-1">
          <?php echo $data['atts']['text-previous'];?>
        </div>
        <div class="line-2">
        <span class="chapter-index"><?php echo $data['pagination']['previous']['index']['value'];?>.&nbsp;</span><?php echo $data['pagination']['previous']['title']['value'];?>
        </div>
      </div>
    </a>
  <?php endif;?>
  <?php if ($data['pagination']['next']):?>
    <a href="/<?php echo I18n::lang() . '/' . $data['pagination']['next']['page_override']['value']['slug'];?>" class="btn btn-default navigation-next">
    <i class="fa fa-chevron-right arrow-icon"></i>
    <div class="text-lines">
        <div class="line-1">
          <?php echo $data['atts']['text-next'];?>
        </div>
        <div class="line-2">
          <span class="chapter-index"><?php echo $data['pagination']['next']['index']['value'];?>.&nbsp;</span><?php echo $data['pagination']['next']['title']['value'];?>
        </div>
      </div>
    </a>
  <?php endif;?>
</div>