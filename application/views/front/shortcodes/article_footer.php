<div class="shortcode shortcode-article-footer<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <span class="author">
    <?php echo $data["entity"]["authors"]["render"];?>
  </span>
</div>