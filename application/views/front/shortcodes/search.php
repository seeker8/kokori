<div class="shortcode shortcode-search<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <?php if ($data['entities']):?>
    <?php foreach($data['entities'] as $result):?>
      <li>
        <a href="<?php echo $result['url'];?>">
        <div class="row">
          <div class="col-lg-3">
            <?php if ($result['image_thumbnail']):?>
              <div class="thumbnail">
                <img src="<?php echo $result['image_thumbnail'];?>" alt="<?php echo $result['title'];?>"/>
              </div>
            <?php endif;?>
          </div>
          <div class="col-lg-9">
            <h2><?php echo $result['title'];?></h2>
            <span class="metas">
              <p><?php echo $result['url'];?></p>
            </span>
            <p><?php echo $result['excerpt'];?></p>
          </div>
        </div>
        </a>
      </li>
    <?php endforeach;?>
  <?php else:?>
    <div class="alert alert-info">
      <?php echo $data['atts']['empty-text'];?>
    </div>
  <?php endif;?>
</div>