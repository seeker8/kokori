<div class="shortcode shortcode-form-contact<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>">
<?php if (isset($data['errors'])):?>
  <?php if (count($data['errors']) > 0):?>
    <div class="alert alert-danger"><?php echo $data['atts']['message-error'];?></div>
  <?php else:?>
    <div class="alert alert-success"><?php echo $data['atts']['message-success'];?></div>
  <?php endif;?>
<?php endif;?>
<form id="form-contact" method="POST">
  <input type="hidden" name="action" value="form_contact"/>
  <div class="form-group">
    <input value="<?php if(isset($data['errors']) && count($data['errors']) > 0):?><?php echo Arr::get($_POST, 'name');?><?php endif;?>" name="name" type="name" class="form-control<?php if (isset($data['errors']['name'])):?> error<?php endif?>"  placeholder="<?php echo $data['atts']['name-label'];?>">
  </div>
<div class="form-group">
<input value="<?php if (isset($data['errors']) && count($data['errors']) > 0):?><?php echo Arr::get($_POST, 'email');?><?php endif;?>" name="email" type="email" class="form-control<?php if (isset($data['errors']['email'])):?> error<?php endif?>" placeholder="<?php echo $data['atts']['email-label'];?>">
  </div>
  <div class="form-group">
    <input value="<?php if(isset($data['errors']) && count($data['errors']) > 0):?><?php echo Arr::get($_POST, 'subject');?><?php endif;?>" name="subject" type="subject" class="form-control<?php if (isset($data['errors']['sub
ject'])):?> error<?php endif?>"  placeholder="<?php echo $data['atts']['subject-label'];?>">
  </div>
  <div class="form-group"><textarea name="message" class="form-control<?php if (isset($data['errors']['message'])):?> error<?php endif?>"  placeholder="<?php echo $data['atts']['message-label'];?>"><?php if(isset($data['errors']) && count($data['errors']) > 0):?><?php echo Arr::get($_POST, 'message');?><?php endif;?></textarea>
  </div>
  <div class="actions">
    <button type="submit" class="btn btn-primary"><?php echo $data['atts']['button-label'];?></button>
  </div>
</form>
</div>
<script src="https://www.google.com/recaptcha/api.js?render=<?php echo $data['atts']['captcha-sitekey'];?>"></script>
<script>
 $(document).ready(function () {
   $('#form-contact button').on('click', function() {
     grecaptcha.execute('<?php echo $data['atts']['captcha-sitekey'];?>',
     {action: 'contact'}).then(function(token) {
	$('#form-contact').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
      	$('#form-contact').trigger('submit');
     });
     return false;
   });
});
  </script>
