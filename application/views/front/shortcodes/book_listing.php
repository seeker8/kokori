<div class="shortcode shortcode-book-listing<?php if ($data['atts']['effect-in']):?> effect-in<?php endif;?>"<?php if ($data['atts']['effect-in']):?> data-effect-in="<?php echo $data['atts']['effect-in'];?>"<?php endif;?>>
  <div data-toggle="isotope" class="row">
    <?php if ($data['listing']):?>
      <?php foreach($data['listing'] as $entity):?>
        <div class="col-md-4 item <?php echo Utils::get_class_filters($entity);?>">
          <span class="alpha d-none"><?php echo $entity["title"]["value"];?></span>
          <a href="<?php echo $entity["page_slug"]["value"];?>" title="<?php echo $entity["title"]["value"];?>">
            <img class="img-fluid" alt="<?php echo $entity["title"]["value"];?>" src="<?php echo $entity["image_thumbnail"]["value"];?>"/>
            <div class="overlay overlay-hue"></div>
            <div class="overlay overlay-color"></div>
            <span class="details">
              <h2 class="title"><?php echo $entity["title"]["value"];?></h2>
              <span class="metas">
                <span class="excerpt">
                  <?php echo $entity["excerpt"]["render"];?>
                </span>
              </span>
            </span>
          </a>
        </div>
      <?php endforeach;?>
    <?php endif;?>
    <?php if ($data['atts']['message-empty']):?>
      <div class="col-md-12 none">
        <div class="alert alert-primary">
          <?php echo $data['atts']['message-empty'];?>
        </div>
      </div>
    <?php endif;?>
  </div>
</div>