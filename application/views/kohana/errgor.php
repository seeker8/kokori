<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Error 500</title>
  <meta name="description" content="Internal Server Error">
  <meta name="author" content="Kokori">
  <link rel="stylesheet" type="text/css" href="/themes/solarus/errors/css/style.css">
</head>
<body class="error">
<div class="container">
  <img src="/themes/solarus/errors/images/kokori_icon.png"/>
  <h1>Error 500</h1>
  <h2>Internal Server Error</h2>
  <p>Something has gone wrong on our website's server.</p>
  <img src="/themes/solarus/errors/images/gears.png"/>
  <p class="small">This website is powered by <a href="http://gitlab.com/solarus-games/kokori">Kokori</a>.</p>
</div>
</div>
</body>
</html>
